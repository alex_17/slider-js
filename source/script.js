import "./style.scss";
import {caruselJs} from './components/carusel/carusel.js';

new caruselJs().init({ 
    id: '.corusel-id',
    nav: '.corusel-nav',
    margin: 40,
    loop: 1,
    item: [
        {
            media: 1024,
            item: 3
        },
        {
            media: 768,
            item: 1
        }
    ],
    speed: 500
});