
export class caruselJs{
    
    constructor(option) {
        this.render = [];
    }

    init(option)
    {
        this.id = !option.id ? '' : option.id;
        this.nav = !option.nav ? '' : option.nav;
        this.margin = !option.margin ? 0 : option.margin;
        this.loop = option.loop;
        this.item = this._item(option);
        this.speed = option.speed / 1000;
        this.speedMilliseconds = option.speed;
        if(this.loop == 0){
            this._loopNull(this);
        }
        if(this.loop == 1){
            this._loopOne(this);
        }
    }

    _loopOne(option)
    {
        let caruselElement = document.querySelector(option.id),
            caruselNav = document.querySelector(option.nav);
            
        let widthcaruselElement = caruselElement.offsetWidth;
    
        let caruselListItem = caruselElement.children,
            lengthCaruselListItem = caruselListItem.length;
        
        caruselElement.style.overflow = 'hidden';
        caruselElement.style.position = 'relative';
    
        let itemWidth = Number(widthcaruselElement / option.item + option.margin / option.item);
        let ransform = 0;
        if(lengthCaruselListItem > option.item){
            ransform = -itemWidth;
        }
        // console.log(itemWidth);
    
        let sI = [];
        for(let i = 0; i < lengthCaruselListItem; i++){
            let divConItem = document.createElement('div');
            divConItem.classList.add('carusel__item');
            divConItem.classList.add('carusel__item_'+i);
            divConItem.style.width = itemWidth+'px';
            divConItem.style.marginRight = option.margin+'px';
            divConItem.innerHTML = caruselListItem[i].outerHTML;
            sI[i] = divConItem;
        }
        this.render = this._river(sI, 'back');
        

        let fullWidthCaruselListItem = Number(itemWidth * this.render.b.length);
        
        let divCarusel = document.createElement('div');
        divCarusel.style.display = 'flex';
        divCarusel.style.width = fullWidthCaruselListItem + 'px';
        divCarusel.style.transform = 'translateX('+ransform+'px)';
        divCarusel.classList.add('carusel');
        for(let i = 0; i < this.render.b.length; i++){
            divCarusel.innerHTML += this.render.b[i].outerHTML; 
        } 
        caruselElement.innerHTML = divCarusel.outerHTML;
    
        if(lengthCaruselListItem > option.item){
            let caruselNavigate = document.querySelector(option.nav);
    
            let divNavigate = document.createElement('div');
            divNavigate.classList.add('carusel__nav');
    
            let divNavigatePrev = document.createElement('div');
            divNavigatePrev.classList.add('carusel__nav__prev');
    
            let divNavigateNext = document.createElement('div');
            divNavigateNext.classList.add('carusel__nav__next');
    
            divNavigate.appendChild(divNavigatePrev)
            divNavigate.appendChild(divNavigateNext)
    
            caruselNavigate.innerHTML = divNavigate.outerHTML;
    
            let caruselNavPrev = document.querySelector(option.nav + ' .carusel__nav__prev');
            let caruselNavNext = document.querySelector(option.nav + ' .carusel__nav__next');
        
            let carusel = document.querySelector(option.id + ' .carusel');
            let granPredCarusel = fullWidthCaruselListItem - (itemWidth * option.item);
            let stopCarusel = granPredCarusel - (granPredCarusel * 2);
    
            let bagsNav = itemWidth/option.item;

            caruselNavPrev.addEventListener('click', (e) => {
                this._caruselAnimation(carusel, ransform, itemWidth, 'back');
            });
    
            caruselNavNext.addEventListener('click', (e) => {
                this._caruselAnimation(carusel, ransform, itemWidth, 'forward');
            });
        }
    }

    _caruselAnimation(carusel, ransform, itemWidth, type)
    {
        let ransformSum = 0;
        switch(type){
            case 'back':
                ransformSum = ransform + itemWidth;
                break;
            case 'forward':
                ransformSum = ransform - itemWidth;
                break;
        }
        var animation = carusel.animate([
            {transform: 'translate('+ransform+'px)'},
            {transform: 'translateX('+ransformSum+'px)'}
          ], {
            duration: this.speedMilliseconds,
            iterations: 1
        })
        
        this.render = this._river(this.render.a, type);
        let renderB = this.render.b;
        animation.addEventListener('finish', function(w) {
            carusel.innerHTML = '';
            renderB.forEach((item) => {
                carusel.innerHTML += item.outerHTML
            })
        });
    }

    _river(sI, type)
    {   
        let result = {};
        result.a = [];
        result.b = [];
        let riv = this.item + 2
        let s = 0;
        switch(type){
            case 'back':
                result.a[s++] = sI[sI.length - 1];
                sI.forEach((item, i) => {
                    if(i < sI.length - 1){
                        result.a[s] = item;
                        s++;
                    } 
                });
                break;
            case 'forward':
                sI.forEach((item, i) => {
                    if(i > 0){
                        result.a[s] = item;
                        s++;
                    } 
                });
                result.a[s++] = sI[0];
                break;
        }       
        result.a.forEach((item, i) => {
            if(i < riv){
                result.b[i] = item;
            }
        });
        return result;
    }
    
    _loopNull(option)
    {
        let ransform = 0;
        let caruselElement = document.querySelector(option.id),
            caruselNav = document.querySelector(option.nav);
            
        let widthcaruselElement = caruselElement.offsetWidth;
    
        let caruselListItem = caruselElement.children,
            lengthCaruselListItem = caruselListItem.length;
    
        caruselElement.style.overflow = 'hidden';
        caruselElement.style.position = 'relative';
    
        let itemWidth = Number(widthcaruselElement / option.item + option.margin / option.item);
    
        let sI = [];
        for(let i = 0; i < lengthCaruselListItem; i++){
            let divConItem = document.createElement('div');
            divConItem.classList.add('carusel__item');
            divConItem.style.width = itemWidth+'px';
            divConItem.style.marginRight = option.margin+'px';
            divConItem.innerHTML = caruselListItem[i].outerHTML;
            sI[i] = divConItem;
        }
    
        let fullWidthCaruselListItem = Number(itemWidth * lengthCaruselListItem);
    
        let divCarusel = document.createElement('div');
        divCarusel.style.display = 'flex';
        divCarusel.style.width = fullWidthCaruselListItem + 'px';
        divCarusel.style.transition = option.speed + 's';
        divCarusel.style.transform = 'translateX('+ransform+')';
        divCarusel.classList.add('carusel');
        for(let i = 0; i < lengthCaruselListItem; i++){
            divCarusel.innerHTML += sI[i].outerHTML; 
        } 
        caruselElement.innerHTML = divCarusel.outerHTML;
    
        if(lengthCaruselListItem > option.item){
            let caruselNavigate = document.querySelector(option.nav);
    
            let divNavigate = document.createElement('div');
            divNavigate.classList.add('carusel__nav');
    
            let divNavigatePrev = document.createElement('div');
            divNavigatePrev.classList.add('carusel__nav__prev');
            divNavigatePrev.classList.add('carusel__nav__prev--none');
    
            let divNavigateNext = document.createElement('div');
            divNavigateNext.classList.add('carusel__nav__next');
    
            divNavigate.appendChild(divNavigatePrev)
            divNavigate.appendChild(divNavigateNext)
    
            caruselNavigate.innerHTML = divNavigate.outerHTML;
    
            let caruselNavPrev = document.querySelector(option.nav + ' .carusel__nav__prev');
            let caruselNavNext = document.querySelector(option.nav + ' .carusel__nav__next');
        
            let carusel = document.querySelector(option.id + ' .carusel');
            let granPredCarusel = fullWidthCaruselListItem - (itemWidth * option.item);
            let stopCarusel = granPredCarusel - (granPredCarusel * 2);
    
            let bagsNav = itemWidth/option.item;

            caruselNavPrev.addEventListener('click', (e) => {
                if(!e.currentTarget.classList.contains('carusel__nav__prev--none')){
                    ransform += itemWidth;
                    carusel.style.transform = 'translateX('+ransform+'px)';
                }
                if(ransform >= -(bagsNav)){ 
                    e.currentTarget.classList.add('carusel__nav__prev--none');
                    e.currentTarget.nextElementSibling.classList.remove('carusel__nav__next--none');
                } else {
                    e.currentTarget.nextElementSibling.classList.remove('carusel__nav__next--none');
                }
            });
    
            caruselNavNext.addEventListener('click', (e) => {
                if(!e.currentTarget.classList.contains('carusel__nav__next--none')){
                    ransform -= itemWidth;
                    carusel.style.transform = 'translateX('+ransform+'px)';
                }
                if(ransform <= (stopCarusel + bagsNav) && ransform >= (stopCarusel - bagsNav)){
                    e.currentTarget.classList.add('carusel__nav__next--none');
                    e.currentTarget.previousElementSibling.classList.remove('carusel__nav__prev--none');
                } else {
                    e.currentTarget.previousElementSibling.classList.remove('carusel__nav__prev--none');
                }
            });
            
        }
    }

    _item(option)
    {
        let item = 1;
        if(option.item){
            if(typeof option.item == 'object'){
                let bag = 0;
                for(let i = 0; i < option.item.length; i++){
                    if(window.innerWidth <= option.item[i].media){
                        item = option.item[i].item;
                        bag++;
                    }
                }
                if(bag == 0){
                    item = option.item[0].item;
                }
            }
            if(typeof option.item == 'number'){
                item = option.item;
            }
        }

        return item;  
    }

}

